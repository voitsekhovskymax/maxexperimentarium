'use strict';


angular.module('myApp.workspace', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/workspace', {
            templateUrl: 'views/workspace.html',
            controller: 'WorkspaceCtrl'
        });

        $routeProvider.when('/workspace2', {
            templateUrl: 'views/workspace/workspace2.html',
            controller: 'WorkspaceCtrl'
        });


        $routeProvider.when('/workspace3', {
            templateUrl: 'views/workspace/workspace3.html',
            controller: 'WorkspaceCtrl'
        });


        $routeProvider.when('/workspace4', {
            templateUrl: 'views/workspace/workspace4.html',
            controller: 'WorkspaceCtrl'
        });
    }])



    .controller('WorkspaceCtrl', function ($scope) {
        $scope.color = "";
        $scope.type = "";
        $scope.actionUrl1 = "";
        $scope.title = "";
        $scope.subtitle = "";
        $scope.who = "";
        $scope.why = "";
        $scope.what = "";
        $scope.purposegoal = "";
        $scope.learningcontent = "";
        $scope.activities = "";
        $scope.learningcourses = "";
        $scope.learninggoals = "";

        $scope.save1 = function (item) {

            var checkarray = document.getElementsByName("r1");
            var flag = false;
            var div = document.getElementById("selecttype");
            for (var i = 0; i < checkarray.length; i++) {
                if (checkarray[i].checked) {
                    flag = true;
                    break;
                }
            }

            if (flag == "") {
                div.style.border = "2px dashed red";
            }
            /* Проверка на поставленные чекбоксы*/
            if (item.color != null) {
                $scope.color = item.color;
                $scope.type = "заглушка";
                $scope.actionUrl1 = "#!/workspace2";

                console.log($scope.color);
            }
            else {
                console.log("No selected radio buttons");
            }
            return flag;
        };


        $scope.save2 = function (item) {
            console.log($scope.color);
            $scope.title = item.title;
            $scope.subtitle = item.subtitle;
            $scope.who = item.who;
            $scope.why = item.why;
            $scope.what = item.what;
            $scope.actionUrl2 = "#!/workspace3";

        };


        $scope.save3 = function (item) {

            $scope.purposegoal = item.purposegoal;
            $scope.learningcontent = item.learningcontent;
            $scope.activities = item.activities;
            $scope.learningcourses = item.learningcourses;
            $scope.learninggoals = item.learninggoals;

            $scope.actionUrl3 = "#!/workspace4";

        };


        $scope.save4 = function (item) {


            /*exports.newCourse = $scope.newCourse;*/

            $scope.actionUrl4 = "#!/courses";
            var course = {};
            course.color = $scope.color;
            course.type = $scope.type;
            course.title = $scope.title;
            course.purposegoal = $scope.purposegoal;
            console.log(course);
            /* console.log($scope.newCourse);*/

        };



    });



