'use strict';

angular.module('myApp.courses', ['ngRoute', 'myApp.courses'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/courses', {
    templateUrl: 'views/courses.html',
    controller: 'CoursesCtrl'
  });
}])

.controller ("CoursesCtrl", [function(){


}]);