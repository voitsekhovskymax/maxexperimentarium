'use strict';




angular.module('myApp', [
    'ngRoute',
    'myApp.workspace',
    'myApp.courses',
    'myApp.version'
]).
config(['$routeProvider', function($routeProvider) {

    $routeProvider.otherwise({redirectTo: '/courses'});
        }])



    .controller('MainController', function($scope) {
        $scope.menu = {};
        $scope.menu.pages = [
            {"url": "/workspace", "description":"Workspace"},
            {"url": "/courses", "description":"Courses"}

        ];

        $scope.menu.isPageSelected = function(page) {
            return ($scope.menu.currentPage === page);
        };

        $scope.menu.SelectPage = function(page) {
            $scope.menu.currentPage = page;
        };
    })